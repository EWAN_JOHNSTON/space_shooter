#pragma once
#include <SFML/Graphics.hpp>

struct spawnData
{
	sf::Vector2f postition;
	std::vector<sf::Vector2f>pattern;
	sf::Time delay;
};