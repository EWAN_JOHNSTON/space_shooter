#include "Enemy.h"

Enemy::Enemy(sf::Texture& enemyTexture, sf::Vector2u newScreenSize,
    std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer, sf::Vector2f startingPosition, std::vector <sf::Vector2f> NewMovementPattern)
    : sprite(enemyTexture)
    , velocity(0.0f, 0.0f)
    , speed(300.0f)
    , screenSize(newScreenSize)
    , bullets(&newBullets)
    , bulletTexture(&newBulletTexture)
    , bulletCooldownRemaining(sf::seconds(0.0f))
    , bulletCooldownMax(sf::seconds(0.5f))
    , bulletFireSound(firingSoundBuffer)
    , movementPattern(NewMovementPattern)
    , currentInstruction(0)
    , alive(true)
{
    sprite.setPosition(startingPosition);
}



void Enemy::Update(sf::Time frameTime)
{
    if (currentInstruction >= movementPattern.size())
    {
        alive = false;
        return;
    }
    //get target from  ins
    sf::Vector2f targetPoint = movementPattern[currentInstruction]; //TODO, get this from instruction list

    //get distance vector from target point
    sf::Vector2f distanceVector = targetPoint - sprite.getPosition();

    //get direction vector 
    float distanceMag = std::sqrt(distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y);
    
    sf::Vector2f directionVector = distanceVector / distanceMag;
    
    //calc new pos
    float distanceToTravel = speed * frameTime.asSeconds();

    sf::Vector2f newPosition = sprite.getPosition() + directionVector*distanceToTravel;

    //check if we will reach or overshoot our target
    if (distanceMag <= distanceToTravel)
    {
        //set new pos directly to target position
        newPosition = targetPoint;
        ++currentInstruction;
    }


    //move the player
    sprite.setPosition(newPosition);

    // Update the cooldown remaining for firing bullets
    bulletCooldownRemaining -= frameTime;


    // If the cooldown is over, spawn a bullet
    if (bulletCooldownRemaining <= sf::seconds(0.0f))
    {
        sf::Vector2f bulletPosition = sprite.getPosition();
        bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
        bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
        bullets->push_back(Bullet(*bulletTexture, screenSize, bulletPosition, sf::Vector2f(-1000, 0)));
        // Play firing sound
        bulletFireSound.play();
        // reset bullet cooldown
        bulletCooldownRemaining = bulletCooldownMax;
    }
}



void Enemy::Draw(sf::RenderWindow& gameWindow)
{
    gameWindow.draw(sprite);
}

bool Enemy::GetAlive()
{
    return alive;
}

sf::FloatRect Enemy::GetHitbox()
{
    return sprite.getGlobalBounds();
}

void Enemy::SetAlive(bool newAlive)
{
    alive = newAlive;
}