#include "Bullet.h"

Bullet::Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPosition, sf::Vector2f newVelocity)
	: sprite(bulletTexture)
	, screenBounds(newScreenBounds)
	, velocity(newVelocity)
	, alive(true)
{
	sprite.setPosition(startingPosition);

}

void Bullet::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	//if the bullet is offscreen
	if (newPosition.x + sprite.getTexture()->getSize().x<0 || newPosition.x> screenBounds.x)
	{
		//TODO: code delete bullet
		alive = false;
	}
	
	//Move to a new position
	sprite.setPosition(newPosition);

}
void Bullet::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}
bool Bullet::GetAlive()
{
	return alive;
}
sf::FloatRect Bullet::GetHitbox()
{
	return sprite.getGlobalBounds();
}
void Bullet::SetAlive(bool newAlive)
{
	alive = newAlive;
}