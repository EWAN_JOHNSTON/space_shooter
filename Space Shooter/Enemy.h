#pragma once
#include <SFML/Graphics.hpp>//Libary needed for using sprites, graphics and fonts
#include <SFML/Audio.hpp>//Libary needed for playing any form of sound
#include <vector>//Libary for handling Collections of objects
#include "Bullet.h"
class Enemy
{
public:
	// Constructor
	Enemy(sf::Texture& playerTexture, sf::Vector2u screenSize,
		std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer, 
		sf::Vector2f startingPosition,std::vector <sf::Vector2f> newMovementPattern);
	void Update(sf::Time frameTime);
	void Draw(sf::RenderWindow& gameWindow);
	//getters
	bool GetAlive();
	sf::FloatRect GetHitbox();
	//setters
	void SetAlive(bool newAlive);
private:

	// Variables (data members) used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	std::vector <sf::Vector2f> movementPattern;
	int currentInstruction;
	bool alive;
};