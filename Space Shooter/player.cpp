#include "player.h"

Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenSize,
    std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer)
    : sprite(playerTexture)
    , velocity(0.0f,0.0f)
    , speed(300.0f)
    , screenSize(newScreenSize)
    , bullets(newBullets)
    , bulletTexture(newBulletTexture)
    , bulletCooldownRemaining(sf::seconds(0.0f))
    , bulletCooldownMax(sf::seconds(0.5f))
    , bulletFireSound(firingSoundBuffer)
{
    
    // Avoid duplicating code by calling reset here instead of positioning the player manually
    Reset(screenSize);
    
}

void Player::Input()
{
    //Character Controls
    velocity.x = 0.0f;
    velocity.y = 0.0f;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        //Move player up
        velocity.y = -speed;

    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        //Move player left
        velocity.x = -speed;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        //Move player down
        velocity.y = speed;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        //Move player right
        velocity.x = speed;
    }

    // If the player is pressing space, spawn a bullet
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)&& bulletCooldownRemaining <= sf::seconds(0.0f))
    {
        sf::Vector2f bulletPosition = sprite.getPosition();
        bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
        bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
        bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(1000, 0)));
        // Play firing sound
        bulletFireSound.play();
        // reset bullet cooldown
        bulletCooldownRemaining = bulletCooldownMax;
    }
}

void Player::Update(sf::Time frameTime)
{
    sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

    if (newPosition.x < 0)
        newPosition.x = 0;

    if (newPosition.x + sprite.getTexture()->getSize().x > screenSize.x)
        newPosition.x = screenSize.x - sprite.getTexture()->getSize().x;

    if (newPosition.y < 0)
        newPosition.y = 0;

    if (newPosition.y + sprite.getTexture()->getSize().y > screenSize.y)
        newPosition.y = screenSize.y - sprite.getTexture()->getSize().y;


    //move the player
    sprite.setPosition(newPosition);

    // Update the cooldown remaining for firing bullets
    bulletCooldownRemaining -= frameTime;
}

void Player::Reset(sf::Vector2u screenSize)
{
    sprite.setPosition(
        screenSize.x / 2 - sprite.getTexture()->getSize().x / 2,
        screenSize.y / 2 - sprite.getTexture()->getSize().y / 2);
    alive = true;

}

void Player::Draw(sf::RenderWindow& gameWindow) 
{
    gameWindow.draw(sprite);
}

bool Player::GetAlive()
{
    return alive;
}

sf::FloatRect Player::GetHitbox()
{
    return sprite.getGlobalBounds();
}

void Player::SetAlive(bool newAlive)
{
    alive = newAlive;
}