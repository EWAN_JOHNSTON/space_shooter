// Library Includes	
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library needed for playing music and sound effects
#include <SFML/Audio.hpp>
// Library for manipulating strings of text
#include <string>
// Library for handling collections of objects
#include <vector>
#include "player.h"
#include "Star.h"
#include "Bullet.h"
#include "Enemy.h"
#include "spawnData.h"


int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	//font
	sf::Font gameFont;
	gameFont.loadFromFile("SSA/Font/mainFont.ttf");

	//music
	sf::Music gameMusic;
	gameMusic.openFromFile("SSA/Audio/music.ogg");
	gameMusic.setVolume(25);
	gameMusic.play();

	//bullets
	std::vector<Bullet> playerBullets;
	std::vector<Bullet> enemyBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("SSA/Graphics/playerBullet.png");
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("SSA/Graphics/enemyBullet.png");
	sf::SoundBuffer playerBulletSound;
	playerBulletSound.loadFromFile("SSA/Audio/Fire.ogg");
	sf::SoundBuffer enemyBulletSound;
	enemyBulletSound.loadFromFile("SSA/Audio/Fire.ogg");


	//Player Setup
	sf::Texture playerTexture;
	playerTexture.loadFromFile("SSA/Graphics/player.png");
	Player playerInstance(playerTexture, 
		gameWindow.getSize(), 
		playerBullets, playerBulletTexture, playerBulletSound);

	//movement patterns
	std::vector <sf::Vector2f> zigZagPattern;
	zigZagPattern.push_back(sf::Vector2f(900, 900));
	zigZagPattern.push_back(sf::Vector2f(600,300));
	zigZagPattern.push_back(sf::Vector2f(300, 900));



	//enemy setup
	sf::Texture enemyTexture;
	enemyTexture.loadFromFile("SSA/Graphics/enemy.png");
	std::vector<Enemy>enemies;

	
	//stars
	sf::Texture starTexture;
	starTexture.loadFromFile("SSA/Graphics/star.png");
	std::vector<Star>stars;
	int numOfStars = 5;
	for (int i = 0; i < numOfStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	//spawn info
	std::vector<spawnData> SDVector;
	int spawnIndex = 0; //tells what spawndata we're on
	SDVector.push_back({ sf::Vector2f(gameWindow.getSize().x - 100, gameWindow.getSize().y / 2), zigZagPattern, sf::seconds(0.0f) });
	SDVector.push_back({ sf::Vector2f(gameWindow.getSize().x-100,100), zigZagPattern, sf::seconds(5.0f)});
	SDVector.push_back({ sf::Vector2f(gameWindow.getSize().x-100,500), zigZagPattern, sf::seconds(5.0f)});
	sf::Time timeToSpawn=SDVector[0].delay;

	//score
	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 20, 60);

	//game over
	bool gameOver = false;

	//game over text
	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER\n\nPress R to restart\nor Q to quit");
	gameOverText.setCharacterSize(72);
	gameOverText.setFillColor(sf::Color::Cyan);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	//lose win jingles
	sf::SoundBuffer lossBuffer;
	lossBuffer.loadFromFile("SSA/Audio/loss.ogg");
	sf::Sound lossSFX(lossBuffer);
	lossSFX.setVolume(30);

	//game win text
	sf::Text gameWinText;
	gameWinText.setFont(gameFont);
	gameWinText.setString("GAME WIN\n\nPress R to restart\nor Q to quit");
	gameWinText.setCharacterSize(72);
	gameWinText.setFillColor(sf::Color::Cyan);
	gameWinText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameWinText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	//win state
	bool winState = false;


	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop
		playerInstance.Input();
		
		if (gameOver || winState)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}
			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::R)))
			{
				gameOver = false;
				winState = false;
				playerInstance.Reset(gameWindow.getSize());
				score = 0;
				gameMusic.play();
				enemies.clear();
				enemyBullets.clear();
				playerBullets.clear();
				spawnIndex = 0;
				timeToSpawn = SDVector[0].delay;
			}
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		if (!gameOver && !winState)
		{
			scoreText.setString("Score: " + std::to_string((int)score));//score update
			playerInstance.Update(frameTime);
			for (int i = 0; i < enemies.size(); i++)
			{
				enemies[i].Update(frameTime);
			}
			//update the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].Update(frameTime);
			}
			//update the enmy bullets
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].Update(frameTime);
			}
			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			//check if it is time to spawn an enemy
			timeToSpawn -= frameTime;
			if (timeToSpawn <= sf::seconds(0) && spawnIndex < SDVector.size())
			{

				enemies.push_back(Enemy(
					enemyTexture,
					gameWindow.getSize(),
					enemyBullets, enemyBulletTexture, enemyBulletSound,
					SDVector[spawnIndex].postition,
					SDVector[spawnIndex].pattern));

				++spawnIndex;
				if (spawnIndex < SDVector.size())
					timeToSpawn = SDVector[spawnIndex].delay;
			}

			//check for collisions between the different bullets PLAYER AGAINST ENEMY
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				for (int j = 0; j < enemies.size(); ++j)
				{
					//get the 2 bounding boxes
					sf::FloatRect bulletBounds = playerBullets[i].GetHitbox();
					sf::FloatRect enemyBounds = enemies[j].GetHitbox();

					//check for overlap
					if (bulletBounds.intersects(enemyBounds))
					{
						//they do

						//Kill the buller and the enemy
						playerBullets[i].SetAlive(false);
						enemies[j].SetAlive(false);

						score += 5;
					}
				}

			}

			//check for collisions between the different bullets ENEMY AGAIST PLAYER
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				//get the 2 bounding boxes
				sf::FloatRect bulletBounds = enemyBullets[i].GetHitbox();
				sf::FloatRect playerBounds = playerInstance.GetHitbox();

				//check for overlap
				if (bulletBounds.intersects(playerBounds))
				{
					//they do

					//Kill the buller and the enemy
					enemyBullets[i].SetAlive(false);
					playerInstance.SetAlive(false);
					gameOver = true;
					gameMusic.stop();
					lossSFX.play();

				}

			}
			//check if won
			if (spawnIndex >= SDVector.size() && enemies.empty())
			{
				winState = true;
				gameMusic.stop();
				//TODO: Add Victory Music
			}

		}
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(15, 15, 15));

		//draw game objects
		gameWindow.draw(scoreText);
		

		// Draw the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].DrawTo(gameWindow);
		}
		
		if (!gameOver && !winState)
		{

			//draw the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].DrawTo(gameWindow);
				// If the bullet is dead, delete it
				if (!playerBullets[i].GetAlive())
				{
					// Remove the item from the vector
					playerBullets.erase(playerBullets.begin() + i);
				}

			}
			//draw the enemy bullets
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].DrawTo(gameWindow);
				// If the bullet is dead, delete it
				if (!enemyBullets[i].GetAlive())
				{
					// Remove the item from the vector
					enemyBullets.erase(enemyBullets.begin() + i);
				}
			}


			// draw the spaceship


			if (playerInstance.GetAlive())
			{
				playerInstance.Draw(gameWindow);
			}

			//draw the enemy
			for (int i = 0; i < enemies.size(); i++)
			{
				enemies[i].Draw(gameWindow);
				if (!enemies[i].GetAlive())
				{
					// Remove the item from the vector
					enemies.erase(enemies.begin() + i);
				}
			}
		}
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}
		if (winState)
		{
			gameWindow.draw(gameWinText);
		}

		// Display the window contents on the screen
		gameWindow.display();

	

	} // End of Game Loop
	return 0;
}