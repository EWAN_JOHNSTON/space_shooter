#include "Star.h"
#include <cstdlib>

Star::Star(sf::Texture& starTexture, sf::Vector2u newScreenBounds)
	:screenBounds(newScreenBounds)
	,speed(500)
{
	sprite.setTexture(starTexture);
	// Avoid duplicating code by calling reset here instead of positioning manually
	Reset();
}

void Star::Update(sf::Time frameTime)
{
	//calculate new posititon
	sf::Vector2f newPosition = sprite.getPosition() + sf::Vector2f(-speed, 0) * frameTime.asSeconds();

	//if it reaches the end of the screen
	if (newPosition.x + sprite.getTexture()->getSize().x < 0)
	{
		// Then wrap around back to the right
		newPosition.x = screenBounds.x;
	}

	//move to the new position
	sprite.setPosition(newPosition);
}

void Star::Reset()
{
	// Choose a random starting position
	sprite.setPosition(sf::Vector2f(rand() % screenBounds.x, rand() % screenBounds.y));
	// Choose a random scale (between 1 and 1/4th scale)
	float scaleFactor = 1.0f / (1.0f + rand() % 4);
	sprite.setScale(scaleFactor, scaleFactor);
}

void Star::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}
